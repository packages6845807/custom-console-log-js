const log = require('./../index');
const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const mensaje = 'Muy buenos dias';
chai.use(sinonChai);

describe('Test de las funciones', () => {

    beforeEach(() => {
        sinon.spy(console, 'log');
    });

    afterEach(() => {
        console.log.restore();
    });

    it('Funcion para Ok', () => {
        log.ok(mensaje);
        expect(console.log).to.be.called;
    });

    it('Funcion para info', () => {
        log.info(mensaje);
        expect(console.log).to.be.called;
    });

    it('Funcion para aviso', () => {
        log.aviso(mensaje);
        expect(console.log).to.be.called;
    });

    it('Funcion para error', () => {
        log.error(mensaje);
        expect(console.log).to.be.called;
    });

});